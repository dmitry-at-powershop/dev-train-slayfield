# Manages game sesssions
class GamesController < ApplicationController
  before_action :find_game, only: [:show, :destroy]

  def index
    @games = Game.all
  end

  def create
    service = CreateGame.new
    service.call
    redirect_to games_path
  end

  def show
    @teams = @game.teams
  end

  def destroy
    @game.destroy
    redirect_to games_path
  end

  private

  def find_game
    @game = Game.find(params[:id])
  end
end

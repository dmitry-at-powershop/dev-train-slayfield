class AddHitLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :characters, :hit_locations, :text, default: '', null: false
  end
end

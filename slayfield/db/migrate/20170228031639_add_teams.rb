class AddTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
      t.string :name
      t.references :game, foreign_key: true
    end

    remove_column :characters, :game_id
    remove_column :characters, :created_at
    remove_column :characters, :updated_at
    add_reference :characters, :team, foreign_key: true
  end
end

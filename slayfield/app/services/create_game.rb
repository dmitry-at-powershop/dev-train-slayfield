# Placeholder for game creation
class CreateGame
  def call
    ActiveRecord::Base.transaction do
      game = Game.new

      team1 = game.teams.new(name: 'Monsters')
      team1.characters.new(name: 'Godzilla', health: 12, power: 3, hit_locations: default_hit_locations)
      team1.characters.new(name: 'King Ghidorah', health: 8, power: 4, hit_locations: default_hit_locations)

      team2 = game.teams.new(name: 'Giant Robots')
      team2.characters.new(name: 'Mechagodzilla', health: 14, power: 5, hit_locations: default_hit_locations)

      game.save!
      game.add_log_entry('Turn 1 has started ')
      game
    end
  end

  private

  def default_hit_locations
    hit_location = {
      7 => :hit,
      8 => :hit,
      9 => :hit,
      10 => :hit,
      11 => :hit,
      12 => :critical,
    }
  end

end

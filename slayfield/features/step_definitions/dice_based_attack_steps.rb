Then(/^I can attack (.*) with (.*)$/) do |target_name, character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    expect(page).to have_button("Attack #{target_name}")
  end
end

When(/^I attack (.*) with (.*)$/) do |target_name, character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    click_button("Attack #{target_name}")
  end
end

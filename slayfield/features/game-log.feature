Feature: Game log
  In order to control the game
  As a player
  I want to see the whole history of actions

  Background:
    Given a default game exists

  Scenario: initial entry
    Given I am on the game page
    Then I can see the log entry: Turn 1 has started

  Scenario: atack entry
    Given I am on the game page
    When I damage Mechagodzilla with King Ghidorah
    Then I can see the log entry: King Ghidorah dealt 4 damage to Mechagodzilla

  Scenario: skip entry
    Given I am on the game page
    When I skip turn with Godzilla
    Then I can see the log entry: Godzilla skipped turn

class AddFinishedTurnToTeams < ActiveRecord::Migration[5.0]
  def change
    add_column :teams, :finished_turn, :boolean, default: false, null: false
  end
end

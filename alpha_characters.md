# Teams

Denis, Ursulia, Rudolf vs Taurus, Snogg, and Dietrich

---

# Denis Krutov

## Basic stats
* Health: 8
* Energy: 2

## Hit locations:
* 12 critical
* 9 - 11 hit
* 6 - 8 armor

## Actions

### Plasma Shot
* Action: aimed attack
* Power: 2
* Range: 5
* Energy: 0

### Grenade Launcher
* Action: area attack
* Power: 3
* Range: 3
* Energy: 1

---

# Rudolf Wolfbane

## Basic stats
* Health: 4
* Energy: 3

## Hit locations:
* 12 critical
* 11 hit
* 8 - 10 armor
* 7 - hit

## Actions

### Shotgun
* Action: regular attack
* Power: 3
* Range: 3
* Energy: 1

### Reload
* Action: energy regeneration
* Power: 3
* Range: 0
* Energy: 0

---

# Ursulia

## Basic stats
* Health: 3
* Energy: 7

## Hit locations:
* 12 hit
* 9 - 11 armor
* 7 - 8 hit

## Actions

### Whithering Trident
* Action: energy drain
* Power: 2
* Range: 4
* Energy: 0

### Energy Gift
* Action: energy regeneration
* Power: 2
* Range: 4
* Energy: 2

---

# Taurus

## Basic stats
* Health: 6
* Energy: 2

## Hit locations:
* 12 critical
* 10 - 11 hit
* 8 - 9 ricochet
* 5 - 7 hit

## Actions

### Ram
* Action: regular attack
* Power: 2
* Range: 1
* Energy: 0

### Flying Hammer
* Action: regular attack
* Power: 2
* Range: 6
* Energy: 1

### Roar
* Action: health drain
* Power: 1
* Range: 4
* Energy: 0

---

# Snogg

## Basic stats
* Health: 4
* Energy: 3

## Hit locations:
* 12 critical
* 8 - 11 hit
* 6 - 7 armor

## Actions

### Tail strike
* Action: aimed attack
* Power: 1
* Range: 1
* Energy: 0

### Heat Ray
* Action: Regular attack
* Power: 2
* Range: 4
* Energy: 1

### Hibernation
* Action: health regeneration
* Power: 2
* Range: 0
* Energy: 0

---

# Dietrich von Messer

## Basic stats
* Health: 5
* Energy: 3

## Hit locations:
* 12 critical
* 11 critical
* 7 - 10 hit

## Actions

### Bite of Nosferatu
* Action: health drain
* Power: 2
* Range: 1
* Energy: 0

### Bat Shriek
* Action: stun
* Power: 1
* Range: 3
* Energy: 1

### Crazy Shredding
* Action: regular attack
* Power: 4
* Range: 1
* Energy: 1

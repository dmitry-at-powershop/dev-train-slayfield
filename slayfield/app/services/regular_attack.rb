# Placeholder for character action
class RegularAttack < ActionService
  def perform_action
    damage = @character.power
    dice_result = rand(1..6) + rand(1..6)
    hit_location = @target.hit_location(dice_result)

    ActiveRecord::Base.transaction do
      damage_dealt = @target.take_damage(modified_damage(damage, hit_location))
      @game.add_log_entry("#{@character.name} attacked #{@target.name}. Dice roll is #{dice_result}. It is a #{hit_location}. #{damage_dealt} health was lost.")
    end
  end

  private

  def modified_damage(damage, hit_location)
    case hit_location
    when :miss
      0
    when :hit
      damage
    when :critical
      2 * damage
    when :armor
      1
    end
  end

end

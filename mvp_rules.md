# Game setup

1. There are two dummy characters.

# Game flow

1. Game goes in turns.
2. Each turn the player activates any character.
3. The results of the activation are shown to the player.
4. Game ends when any character is eliminated.

# What is character

0. Name and description
1. Image
2. Hit locations table (2-12)
3. Maximal and current health
5. Actions

# Hit locations
* miss
* hit

# What is action
* Name
* Power

# Ability types
* Regular attack

# Character activation flow

## Choosing a target

## Starting action
* Dice result is calculated (2d6).

## Determining success to the target

* The target's hit location is determined.
* If the hit location is a miss, finish the action against this target.

## Applying damage the target
* If the hit location is hit, the damage value is set to action's power.
* Target looses health equal to the damage value.

## Adjusting target's health and energy
* If target's health is zero or less, target is eliminated.

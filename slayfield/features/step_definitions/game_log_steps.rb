Then(/^I can see the log entry: (.*)$/) do |entry|
  within "div#log" do
    expect(page).to have_content(entry)
  end
end

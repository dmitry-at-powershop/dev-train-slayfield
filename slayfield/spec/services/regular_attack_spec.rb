RSpec.describe RegularAttack do

  describe 'modified_damage' do
    let(:service) { RegularAttack.new(:game, :@character, :target) }
    context 'given damage and hit location' do
      it 'should return the resulting damage' do
        expect(service.send(:modified_damage, 10, :miss)).to eql 0
        expect(service.send(:modified_damage, 10, :hit)).to eql 10
        expect(service.send(:modified_damage, 10, :critical)).to eql 20
      end
    end
  end

  describe 'perform_action' do

    before do
      @game = instance_double("Game")
      allow(@game).to receive(:add_log_entry)
      @character = instance_double("Character", :name => 'Attacker', :power => 7)
      @target = spy("Character", :name => 'Target')
    end

    context 'given hit location is a hit' do
      before do
        allow(@target).to receive(:hit_location).and_return(:hit)
        service = RegularAttack.new(@game, @character, @target)
        service.perform_action
      end
      it 'results in full damage to target' do
        expect(@target).to have_received(:take_damage).with(7)
      end
    end

    context 'given hit location is a miss' do
      before do
        allow(@target).to receive(:hit_location).and_return(:miss)
        service = RegularAttack.new(@game, @character, @target)
        service.perform_action
      end
      it 'results in full damage to target' do
        expect(@target).to have_received(:take_damage).with(0)
      end
    end

    context 'given hit location is a critical' do
      before do
        allow(@target).to receive(:hit_location).and_return(:critical)
        service = RegularAttack.new(@game, @character, @target)
        service.perform_action
      end
      it 'results in full damage to target' do
        expect(@target).to have_received(:take_damage).with(14)
      end
    end

    context 'given hit location is an armor' do
      before do
        allow(@target).to receive(:hit_location).and_return(:armor)
        service = RegularAttack.new(@game, @character, @target)
        service.perform_action
      end
      it 'results in full damage to target' do
        expect(@target).to have_received(:take_damage).with(1)
      end
    end
  end


end

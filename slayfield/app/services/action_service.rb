# Placeholder for character action
class ActionService
  def initialize(game, character, target)
    @game = game
    @character = character
    @target = target
  end


  def self.by_type(type, game, character, target)
    case type
    when :regular_attack
      RegularAttack.new(game, character, target)
    when :damage
      DealDirectDamage.new(game, character, target)
    when :skip
      SkipTurn.new(game, character, nil)
    else
      raise "Unsupported type of action: #{type}"
    end
  end

  def call
    return false unless @character.alive?
    return false if @character.team.finished_turn

    perform_action

    @character.team.update!(finished_turn: true)
    @game.manage_turns

  end

  private

  def perform_action; end
end


Given(/^a default game exists$/) do
  service = CreateGame.new
  @game = service.call
  srand(0)
end

When(/^I am on the game page$/) do
  visit game_path(@game)
end

Then(/^I can see team (.*) with (.*)$/) do |team_name, character_name|
  team = @game.teams.find_by(name: team_name)
  expect(page).to have_content("Team: #{team.name}")
  within "div#team_#{team.id}" do
    expect(page).to have_content(character_name)
  end
end

Then(/^I can see ([^']+)'s (health|power) is (\d+)$/) do |expected_name, stat, expected_value|
  character = @game.character_by_name(expected_name)
  name = page.find_by_id("character_#{character.id}_name").text
  stat_value = page.find_by_id("character_#{character.id}_#{stat}").text
  expect(name).to eql(expected_name)
  expect(stat_value).to eql(expected_value)
end

When(/^I damage (.*) with (.*)$/) do |target_name, character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    click_button("Damage #{target_name}")
  end
end

Then(/^I can see (.*) dead$/) do |target_name|
  target = @game.character_by_name(target_name)
  within "div#character_#{target.id}" do
    expect(page).to have_content('DEAD')
  end
end

Then(/^I can see team (.*) won$/) do |team_name|
  expect(page).to have_content("#{team_name} WON!")
end

Then(/^turn is (\d+)$/) do |turn|
  expect(page).to have_content("turn #{turn}")
end

Then(/^I can damage (.*) with (.*)$/) do |target_name, character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    expect(page).to have_button("Damage #{target_name}")
  end
end

Then(/^I can't damage (.*) with (.*)$/) do |target_name, character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    expect(page).not_to have_button("Damage #{target_name}")
  end
end

When(/^I skip turn with (.*)$/) do |character_name|
  character = @game.character_by_name(character_name)
  within "div#character_#{character.id}" do
    click_button("Skip turn")
  end
end

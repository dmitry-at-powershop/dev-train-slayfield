# Placeholder for character action
class SkipTurn  < ActionService
  def perform_action
    ActiveRecord::Base.transaction do
      @character.team.update!(finished_turn: true)
      @game.add_log_entry("#{@character.name} skipped turn")
    end
  end
end

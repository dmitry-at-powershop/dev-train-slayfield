# This implements character class (with no character card)
class Game < ApplicationRecord
  has_many :teams, dependent: :destroy
  has_many :logs, dependent: :destroy
  validate :require_two_teams

  def winner
    alive_teams = teams.find_all{ |t| t.has_alive_characters }
    alive_teams.count == 1 ? alive_teams.first : nil
  end

  def characters
    teams.reduce([]){ |all, t| all += t.characters }
  end

  def character_by_name(name)
    characters.find{ |c| c.name == name }
  end

  def manage_turns
    unless teams.exists?(finished_turn: false) then
      ActiveRecord::Base.transaction do
        teams.each do |t|
          t.finished_turn = false
          t.save!
        end
        self.increment('turn')
        add_log_entry("Turn #{turn} has started")
        save!
      end
    end
  end

  def add_log_entry (action)
    self.logs.create(turn: turn, action: action, step: step)
  end

  private

  def step
    self.logs.where(turn: turn).count
  end

  def require_two_teams
    raise('Game must contain at least two teams') if teams.size < 2
  end
end

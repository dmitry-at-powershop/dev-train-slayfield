Feature: teams damage in turns
  In order to play in teams
  As a player
  I want the teams to be able to damage once per turn

  Background:
    Given a default game exists

  Scenario: any team can damage initially
    Given I am on the game page
    Then turn is 1
    And I can damage Mechagodzilla with Godzilla
    And I can damage Mechagodzilla with King Ghidorah
    And I can damage Godzilla with Mechagodzilla
    And I can damage King Ghidorah with Mechagodzilla

  Scenario: team can not damage two times in the same turn
    Given I am on the game page
    When I damage Mechagodzilla with King Ghidorah
    Then turn is 1
    And I can't damage Mechagodzilla with Godzilla
    And I can't damage Mechagodzilla with King Ghidorah
    And I can damage Godzilla with Mechagodzilla
    And I can damage King Ghidorah with Mechagodzilla

  Scenario: attacking by both teams changes the turn
    Given I am on the game page
    When I damage Mechagodzilla with King Ghidorah
    And I damage Godzilla with Mechagodzilla
    Then turn is 2
    And I can damage Mechagodzilla with Godzilla
    And I can damage Mechagodzilla with King Ghidorah
    And I can damage Godzilla with Mechagodzilla
    And I can damage King Ghidorah with Mechagodzilla

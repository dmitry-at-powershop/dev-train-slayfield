Feature: Games CRUD
  In order to manage games
  As a player
  I want create, play and remove games

  Scenario: Viewing the games list
    Given a game exists
    When I am on the games list page
    Then I can view the list of games

  Scenario: Creating a game
    When I am on the games list page
    Then I can start a new game

  Scenario: Viewing a game
    Given a game exists
    When I am on the games list page
    Then I can view a game

  Scenario: Removing a game
    Given a game exists
    When I am on the games list page
    Then I can remove a game

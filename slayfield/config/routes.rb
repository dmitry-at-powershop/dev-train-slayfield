Rails.application.routes.draw do
  resources :games, only: [:index, :show, :create, :destroy] do
    resources :teams, only: [:index, :show, :create] do
      resources :characters, only: [:index, :create] do
        resources :actions, only: [:create]
      end
    end
  end

  root to: 'games#index'
end

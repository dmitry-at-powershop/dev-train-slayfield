Feature: Dice based attack
  To make the game less predetermined
  As a player
  I want to add attacks based on dice roll

  Background:
    Given a default game exists

  Scenario: being able to attack
    Given I am on the game page
    Then I can attack Mechagodzilla with Godzilla

  Scenario: attacking other character
    Given I am on the game page
    When I attack Mechagodzilla with Godzilla
    Then I can see the log entry: Godzilla attacked Mechagodzilla. Dice roll is 11. It is a hit. 3 health was lost.
    And I can see Mechagodzilla's health is 11

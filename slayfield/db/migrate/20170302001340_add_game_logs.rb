class AddGameLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :logs do |t|
      t.integer :step
      t.integer :turn
      t.string :action
      t.references :game, foreign_key: true

      t.timestamps
    end
  end
end

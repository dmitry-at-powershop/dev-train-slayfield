Feature: Simple play
  In order to play a simple game with basic attacks only
  As a player
  I want to be able to damage until only one team has alive character.

  Background:
    Given a default game exists

  Scenario: viewing teams
    Given I am on the game page
    Then I can see team Monsters with Godzilla
    And I can see team Monsters with King Ghidorah
    And I can see team Giant Robots with Mechagodzilla
    And I can't damage King Ghidorah with Godzilla
    And I can't damage Godzilla with King Ghidorah

  Scenario: viewing initial monsters state
    Given I am on the game page
    Then I can see Godzilla's health is 12
    And I can see Godzilla's power is 3
    And I can see King Ghidorah's health is 8
    And I can see King Ghidorah's power is 4
    And I can see Mechagodzilla's health is 14
    And I can see Mechagodzilla's power is 5

  Scenario: attacking with a character
    Given I am on the game page
    When I damage Mechagodzilla with King Ghidorah
    Then I can see Mechagodzilla's health is 10
    And I can see King Ghidorah's health is 8

  Scenario: killing a target
    Given I am on the game page
    When I damage King Ghidorah with Mechagodzilla
    And I skip turn with Godzilla
    And I damage King Ghidorah with Mechagodzilla
    Then I can see King Ghidorah's health is 0
    And I can see King Ghidorah dead
    And I can't damage King Ghidorah with Mechagodzilla

  Scenario: winning the game
    Given I am on the game page

    When I damage Mechagodzilla with Godzilla
    And I damage King Ghidorah with Mechagodzilla

    And I damage Mechagodzilla with Godzilla
    And I damage King Ghidorah with Mechagodzilla

    And I damage Mechagodzilla with Godzilla
    And I damage Godzilla with Mechagodzilla

    And I damage Mechagodzilla with Godzilla
    And I damage Godzilla with Mechagodzilla

    And I damage Mechagodzilla with Godzilla
    Then I can see King Ghidorah dead
    And I can see Mechagodzilla dead
    And I can see team Monsters won

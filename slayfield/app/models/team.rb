# This implements character class (with no character card)
class Team < ApplicationRecord
  belongs_to :game
  has_many :characters, dependent: :destroy
  validate :require_characters

  def has_alive_characters
    characters.select{ |c| c.alive? }.count > 0
  end

  private

  def require_characters
    raise('Game must contain at least one characters') unless characters
  end
end

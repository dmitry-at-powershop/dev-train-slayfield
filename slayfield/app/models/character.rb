# Character prototype
class Character < ApplicationRecord
  belongs_to :team
  validates_numericality_of :health, only_integer: true, greater_than_or_equal_to: 0
  serialize :hit_locations, JSON

  def take_damage(damage)
    damage = self.health if self.health < damage
    self.decrement('health', damage)
    save!
    damage
  end

  def valid_targets
    # opposing_teams = team.game.teams.select{|t| t.id != self.team.id}
    opposing_teams = team.game.teams.where.not(id: self.team.id)
    all_opposing_characters = opposing_teams.reduce([]){ |all, t| all += t.characters }
    all_opposing_characters.select(&:alive?)
  end

  def hit_location(dice_result)
    (hit_locations[dice_result.to_s]||:miss).to_sym
  end

  def alive?
    health > 0
  end
end

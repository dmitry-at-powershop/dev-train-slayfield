class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.integer :turn, default: 1, null: false

      t.timestamps
    end
  end
end

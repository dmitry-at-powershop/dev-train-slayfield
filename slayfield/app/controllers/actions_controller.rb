# Executing attacks for a @character in a resty way
class ActionsController < ApplicationController
  before_action :find_objects, only: [:create]

  def create
    service = ActionService.by_type(@type, @game, @character, @target)
    service.call
    redirect_to @game
  end

  private

  def find_objects
    @game = Game.find(params[:game_id])
    @character = Character.find(params[:character_id])
    @type = params[:type].to_sym
    @target = Character.find_by(id: params[:target_id])
  end
end

# Game setup

1. There are two opposing teams.
2. Both teams are controlled by a single player.
3. Teams consist of fixed sets of characters.


# Game flow

1. Game goes in turns.
2. Each turn the player activates each character once.
3. The results of the activation are shown to the player.
4. Game ends when any team losses all characters.


# What is character

0. Name
1. Image
2. Hit locations table (2-12)
3. Maximal health
4. Maximal energy
5. Abilities

6. Current health
7. Current energy
8. Stun markers
9. Activated


# Hit locations

* miss
* hit
* armor
* ricochet
* critical

# What ability consists of

* Name
* Type
* Range
* Energy
* Power


# Ability types

* Regular attack
* Area attack
* Aimed attack
* Stun attack
* Energy drain attack
* Health drain attack

* Energy regeneration
* Health regeneration


# Character activation flow

## Choosing and checking an ability

* If the character has a stun marker, the only ability available is to remove the stun marker.
* The player chooses the ability
* Checking that character has more energy than ability's energy cost

## Choosing a target

* If the ability has no range, the target is the character himself.
* If the ability has range, then player chooses a target character.
* If the ability is an area attack, every opposing character becomes target as well.

## Starting ability

* The character's energy is decreased by the ability's energy cost
* Dice result is calculated (2d6).
* If the ability is an aimed attack the dice result is increased by 1 (to a maximum of 12).

## For each target

### Determining success to the target

* The target's hit location is determined.
* If the hit location is a miss, finish the ability use against this target.

### Applying damage the target

* If ability is an attack and the hit location is hit, the damage value is set to ability's power.
* If ability is an attack with non-zero power and the hit location is armor, the damage value is set to 1.
* If ability is an attack and the hit location is critical, the damage value is the doubled ability's power.
* If ability is an attack and the hit location is ricochet, the damage value is set to the ability's power. Character looses health equal to the damage value.
* Target looses health equal to the damage value.

### Applying special features of the ability to the target

* If the ability is an energy drain, character gets energy equal to target's lost health.
* If the ability is a health drain, character gets health equal to target's lost health.
* If the ability is a stun attack, target receives a stun marker.
* If the ability is an energy regeneration, the target gets energy equal to the ability's power.
* If the ability is a health regeneration, the target gets health equal to the ability's power.

### Adjusting target's health and energy

* If target's health is more than maximum, make it maximum.
* If target's energy is more than maximum, make it maximum.
* If target's health is zero or less, target is eliminated.

## Adjusting character's health and energy

* If character's health is more than maximum, make it maximum.
* If character's energy is more than maximum, make it maximum.
* If character's health is zero or less, character is eliminated.

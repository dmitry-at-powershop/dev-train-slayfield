Given(/^a game exists$/) do
  CreateGame.new.call
end

When(/^I am on the games list page$/) do
  visit games_path
end

Then(/^I can view the list of games$/) do
  expect(page).to have_xpath('/html/body/table/tr[2]')
end

Then(/^I can start a new game$/) do
  click_button('New Game')
  expect(page).to have_xpath('/html/body/table/tr[2]')
end

Then(/^I can view a game$/) do
  expect(page).to have_link('Show')
  click_link('Show')
  expect(page).to have_content('Game')
  expect(page).to have_content('turn')
end

Then(/^I can remove a game$/) do
  expect(page).to have_link('Clear')
  click_link('Clear')
  expect(page).not_to have_xpath('/html/body/table/tr[2]')
end

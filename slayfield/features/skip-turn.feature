Feature: Skip turn
  For future benefits and ease of testing
  As a player
  I want to be able to skip a turn with any character

  Background:
    Given a default game exists

  Scenario: character skips his turn
    Given I am on the game page
    When I skip turn with Godzilla
    Then turn is 1
    And I can't damage Mechagodzilla with King Ghidorah
    And I can damage Godzilla with Mechagodzilla
    And I can damage King Ghidorah with Mechagodzilla

  Scenario: both teams skip theirs turns
    Given I am on the game page
    When I skip turn with Mechagodzilla
    And I skip turn with Godzilla
    Then turn is 2
    And I can damage Mechagodzilla with Godzilla
    And I can damage Mechagodzilla with King Ghidorah
    And I can damage Godzilla with Mechagodzilla
    And I can damage King Ghidorah with Mechagodzilla

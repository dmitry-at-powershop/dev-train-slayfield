# Placeholder for character action
class DealDirectDamage < ActionService
  def perform_action
    ActiveRecord::Base.transaction do
      damage_dealt = @target.take_damage(@character.power)
      @game.add_log_entry("#{@character.name} dealt #{damage_dealt} damage to #{@target.name}")
    end
  end
end
